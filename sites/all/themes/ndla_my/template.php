<?php

function ndla_my_add_icon($class = FALSE, $html = FALSE, $title = FALSE) {
  global $user;
  static $icons = array();
  if ($class && $html) {
    $icons[] = array(
      'class' => $class,
      'html' => $html,
      'title' => $title,
    );
  }
  return $icons;
}

/**
 * Implementation of hook_preprocess_page().
 */
function ndla_my_preprocess_page(&$vars) {
  global $user;

  $vars['icons'] = ndla_my_add_icon();

  $element = array(
    '#type' => 'markup',
    '#markup' => '<!--[if lte IE 8]><script type="text/javascript" src="/' . drupal_get_path('theme', 'ndla_my') . '/js/IE9.js"></script><![endif]-->',
  );
  drupal_add_html_head($element, 'IE9');

  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.draggable');
  drupal_add_library('system', 'effects');
  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('theme', 'ndla_my') . '/libraries/chosen/chosen/chosen.jquery.min.js');
  drupal_add_css(drupal_get_path('theme', 'ndla_my') . '/css/chosen.css');

  drupal_add_js(drupal_get_path('theme', 'ndla_my') . '/libraries/jscrollpane/jquery.jscrollpane.min.js');
  drupal_add_js(drupal_get_path('theme', 'ndla_my') . '/libraries/jscrollpane/jquery.mousewheel.js');
  drupal_add_js(drupal_get_path('theme', 'ndla_my') . '/libraries/jscrollpane/mwheelIntent.js');
  drupal_add_css(drupal_get_path('theme', 'ndla_my') . '/libraries/jscrollpane/jquery.jscrollpane.css');

  if (module_exists('ndla_auth')) {
    $vars['auth_user'] = array(
      'display_name' => htmlspecialchars(ndla_auth_display_name()),
      'profile_picture' => ndla_auth_profile_picture(),
    );
    $vars['login_text'] = $user->uid ? t('Log out') : t('Log in');
    $vars['login_url'] = $user->uid ? ndla_auth_logout_url() : ndla_auth_login_url();
  } else {
    $vars['auth_user'] = FALSE;
    $vars['login_text'] = '&nbsp;';
    $vars['login_url'] = '';
    drupal_set_message('This website will not work without the ndla_auth module enabled', 'error');
  }
}

/**
 * Implementation of hook_library_alter().
 */
function ndla_my_css_alter(&$css) {
  if (isset($css['misc/ui/jquery.ui.tabs.css'])) {
    $css['misc/ui/jquery.ui.tabs.css']['data'] = drupal_get_path('theme', 'ndla_my') . '/libraries/jquery_ui/jquery.ui.tabs.css';
  }
  if (isset($css['misc/ui/jquery.ui.theme.css'])) {
    $css['misc/ui/jquery.ui.theme.css']['data'] = drupal_get_path('theme', 'ndla_my') . '/libraries/jquery_ui/jquery.ui.theme.css';
  }
}

function ndla_my_menu_link($variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '><div class="menu-item-top"></div><div class="menu-item-middle">' . $output . $sub_menu . '</div><div class="menu-item-bottom"></div></li>';
}

function ndla_my_preprocess_html(&$vars) {
  drupal_add_css(drupal_get_path('theme', 'ndla_my') . '/css/font-awesome.min.css', array(
    'group' => CSS_THEME,
    'preprocess' => TRUE
  ));
  drupal_add_css(drupal_get_path('theme', 'ndla_my') . '/css/font-awesome-ie7.min.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => 'lte IE 7',
      '!IE' => FALSE
      ),
    'preprocess' => FALSE
  ));
  $element = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => '//fonts.googleapis.com/css?family=Source+Sans+Pro',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
  );
  drupal_add_html_head($element, 'googlefont');
}
