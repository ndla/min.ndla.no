jQuery(document).ready(function($) {

  $('.submenu').click(function() {
    $(this).toggleClass('open');
  });

  $('#navigation > ul > li:not(.submenu) a').click(function() {
    var id = $(this).data('id');
    var visible = $('#message-' + id).is(':visible');
    $('#messages').slideUp(function() {
      if(!visible) {
        $('#messages > div').hide();
        $('#message-' + id).show();
        $('#messages').slideDown();
      }
    });
  });

});