<header id="header">
  <a href="<?php print base_path(); ?>"><img src="/<?php print $directory;?>/images/logo.png" alt="" id="logo" class="logo"></a>
	<nav id="navigation">
	  <ul>
      <?php foreach($icons as $key => $icon): ?>
			  <li><a href='javascript:void(0);' data-id="<?php print $key; ?>" title="<?php print $icon['title']; ?>"><i class="fa <?php print $icon['class']; ?>"></i></a></li>
      <?php endforeach; ?>
      
      <?php if(user_is_logged_in()): ?>
			<li class="submenu">        
        <a href='javascript:void(0);'><?php if ($auth_user) print ($auth_user['profile_picture'] ? '<div id="user-icon"><img src="' . $auth_user['profile_picture'] . '&width=25&height=25&method=scaledowncrop"></div>' : '') . t('Welcome') . ' ' . $auth_user['display_name'];?></a>
		    <ul class="dropdown">
					<li><a href="<?php print base_path(); ?>usersettings/usersettings"><i class="fa fa-user"></i><?php print t('My User'); ?></a></li>
					<li><a href="<?php print base_path(); ?>seriaauth/logout"><i class="fa fa-sign-out"></i><?php print t('Logout'); ?></a></a></li>
				</ul>
			</li>
      <?php else: ?>
        <li><a href="<?php print base_path(); ?>seriaauth/authenticate"><i class="fa fa-sign-in"></i> <?php print t('Login'); ?></a></li>
      <?php endif; ?>
		</ul>
	</nav>
</header>
<div id='messages'>
<?php foreach($icons as $key => $icon): ?>
  <div id='message-<?php print $key; ?>'>
    <?php print $icon['html']; ?>
  </div>
<?php endforeach; ?>
</div>

<?php if ($page['content_top']): ?><?php print render($page['content_top']); ?><?php endif; ?>

<?php print render($page['content']); ?>