<?php

function ndla_my_h5p_get_h5pdata() {
  global $user;

  $query = db_select('ndla_my_h5p', 'nmh');
  $query->join('ndla_msservices_sites', 'nms', 'nmh.site_id = nms.site_id');
  $query->fields('nmh')
        ->fields('nms', array('url'))
        ->condition('uid', $user->uid, '=');
  $result = $query->execute();

  $rows = array();
  while ($record = $result->fetchObject()) {
    $page_data = json_decode($record->page_data);
    
    /* Make sure all fields are set */
    foreach(array('subject', 'title', 'contenttype') as $field) {
      if(!isset($page_data->$field)) {
        $page_data->$field = ''; 
      }
    }

    preg_match('/^([^\/]*)\/node/', $record->page, $matches);
    $lang = $matches[1];

    $rows[] = array(
      'id' => $record->id,
      'url' => $record->url . '/' . $record->page,
      'title' => $page_data->title,
      'data' => $page_data->data,
      'subject' => $page_data->subject,
      'contenttype' => $page_data->contenttype,
      'language' => $lang,
    );
  }

  return $rows;
}


function ndla_my_h5p_display_widget() {
  global $user, $base_path, $language;
  
  /* Run actions */
  if(!empty($_GET['action'])) {
    switch($_GET['action']) {
      case 'delete':
      
        /* Get id */
        $id = $_GET['params']['id'];
        db_delete('ndla_my_h5p')
         ->condition('id', $id)
         ->condition('uid', $user->uid)
         ->execute();
    }
  }

  /* Run post actions */
  if(!empty($_POST['action'])) {
    switch($_POST['action']) {
      case 'delete_widget':
        db_delete('ndla_my_h5p')
         ->condition('uid', $user->uid)
         ->execute();
    }
  }
  
  /* Get view */
  $full = (!empty($_GET['view']) && $_GET['view'] == 'full');

  
  /* Get H5P data */
  $h5pdata = ndla_my_h5p_get_h5pdata();

  /* Build output json */
  $out = array(
    'type' => 'table',
    'data' => array(
      'headers' => array(
        array(
          'text' => t('Page title'),
          'sortable' => TRUE
        ),
      ),
      'rows' => array(
      ),
      'more' => (!$full) ? t('Full view') : FALSE,
    ),
  );
  if($full) {
    $out['data']['headers'][] = array(
      'text' => t('Subject'),
      'sortable' => TRUE,
    );
    $out['data']['headers'][] = array(
      'text' => t('Content type'),
      'sortable' => TRUE,
    );
  }
  $out['data']['headers'][] = array(
    'text' => t('Text'),
    'sortable' => TRUE,
  );
  if($full) {
    $out['data']['headers'][] = array();
  }
  
  foreach($h5pdata as $data) {
    $row = array(
      array(
        'text' => $data['title'],
        'link' => $data['url'] . '?load=1'
      ),
    );
    if(!empty($data['language']) && !in_array($data['language'], array('und', $language->language))) {
      $row[0]['lang'] = $data['language'];
    }
    if($full) {
      $row[] = array(
        'text' => $data['subject'],
      );
      $row[] = array(
        'text' => $data['contenttype'],
      );
    }
    $row[] = array(
      'text' => !empty($data['data'][0]) ? $data['data'][0] : '',
    );
    if($full) {
      $row[] = array(
        'text' => t('Delete'),
        'action' => 'delete',
        'params' => 'id=' . $data['id'],
      );
    }
    $out['data']['rows'][] = $row;
  }

  /* Print result */
  drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
  echo $_GET['callback'] . '(' . json_encode($out) . ')';
 
  die();
}
