<?php

function ndla_my_h5p_results_widget() {
  global $user, $base_path;

  // Get view mode
  $full = (!empty($_GET['view']) && $_GET['view'] == 'full');

  // Build widget data structure
  $widget = array(
    'type' => 'table',
    'data' => array(
      'headers' => array(
        array(
          'text' => t('Title'),
          'sortable' => TRUE
        ),
        array(
          'text' => t('Course'),
          'sortable' => TRUE
        ),
        array(
          'text' => t('Score'),
          'sortable' => TRUE
        ),
      ),
      'rows' => array(),
      'more' => (!$full ? t('Full view') : FALSE),
    ),
  );

  // Fields
  $fields = array('path', 'title', 'course', 'score', 'max_score');

  if ($full) {
    // Add full view headers
    $widget['data']['headers'][] = array(
      'text' => t('Finished'),
      'sortable' => TRUE,
    );

    // Add full view fields
    $fields[] = 'finished';
  }

  // Get result datas
  $query = db_select('ndla_my_h5p_results', 'hr');
  $query->join('ndla_msservices_sites', 'nms', 'hr.site_id = nms.site_id');
  $query->fields('hr', $fields)
    ->fields('nms', array('url'))
    ->condition('user_id', $user->uid)
    ->orderBy('finished', 'DESC');
  // TODO: Limit? pagination?
  $query_result = $query->execute();

  // Build rows from result data
  while ($result = $query_result->fetchObject()) {
    $row = array(
      array(
        'link' => check_url($result->url . '/' . $result->path),
        'text' => check_plain($result->title)
      ),
      array(
        'text' => check_plain($result->course)
      ),
      array(
        'text' => intval($result->score) . '/' . intval($result->max_score)
      )
    );

    if ($full) {
      $row[] = array(
        'text' => format_date($result->finished, 'short')
      );
    }

    $widget['data']['rows'][] = $row;
  }

  // Print results
  drupal_add_http_header('Content-Type', 'application/json');
  echo $_GET['callback'] . '(' . json_encode($widget) . ')';
}
