<?php

/**
 * Implements hook_menu().
 *
 * Services UI is defined in the export-ui plugin.
 */
function ndla_annotate_server_menu() {
  $items = array();
  $items['annotate/widget'] = array(
    'title' => 'My Annotations',
    'description' => 'See annotations made.',
    'page callback' => 'ndla_annotate_server_display_widget',
    'access callback' => 'ndla_annotate_server_has_access',
    'type' => MENU_LOCAL_TASK
  );

  return $items;
}

function ndla_annotate_server_has_access() {
  global $user;
  if (!$user->uid && $_GET['rau']) {
    seriaauth_roamauth($_GET['rau']);
  }
  return ($user->uid == TRUE);
}

function ndla_annotate_server_display_widget() {
  global $user, $base_path, $language;

  $cid = "annotate_widget:" . $user->uid;

  /* Run actions */
  if(!empty($_GET['action'])) {
    switch($_GET['action']) {
      case 'delete':
        /* Get id */
        $id = $_GET['params']['id'];
        db_delete('ndla_annotate_server')
         ->condition('id', $id)
         ->condition('uid', $user->uid)
         ->execute();
        // Invalidate all cached versions
        cache_clear_all($cid . ":", 'cache', TRUE);
        break;
    }
  }

  /* Run post actions */
  if(!empty($_POST['action'])) {
    switch($_POST['action']) {
      case 'delete_widget':
        db_delete('ndla_annotate_server')
         ->condition('uid', $user->uid)
         ->execute();
        // Invalidate all cached versions
        cache_clear_all($cid . ":", 'cache', TRUE);
        break;
    }
  }

  $full = (!empty($_GET['view']) && $_GET['view'] == 'full');

  /* Get annotations */
  $annotations = ndla_annotate_server_get_annotations();

  /* Build output json */
  $out = array(
    'type' => 'table',
    'data' => array(
      'headers' => array(
        array(
          'text' => t('Page title'),
          'sortable' => TRUE
        ),
        array(
          'text' => t('Note'),
          'sortable' => TRUE
        ),
      ),
      'rows' => array(
      ),
      'more' => (!$full) ? t('Full view') : FALSE,
    ),
  );
  if($full) {
    $out['data']['headers'][] = array();
  }

  foreach ($annotations as $annotation) {
    $row = array(
      array(
        'text' => $annotation['title'],
        'link' => $annotation['url']
      ),
      array(
        'text' => $annotation['text'],
      ),
    );
    if(!empty($annotation['language']) && !in_array($annotation['language'], array('und', $language->language))) {
      $row[0]['lang'] = $annotation['language'];
      $row[1]['lang'] = $annotation['language'];
    }
    if($full) {
      $row[] = array(
        'text' => t('Delete'),
        'action' => 'delete',
        'params' => 'id=' . $annotation['id'],
      );
    }
    $out['data']['rows'][] = $row;
  }

  /* Print result */
  drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
  echo $_GET['callback'] . '(' . json_encode($out) . ')';
  die();
}

/**
 * Implementation of hook_msservices_get()
 */
function ndla_annotate_server_msservices_get($fields) {
  $page = $fields['language'] . '/' . $fields['page'];
  $site_id = ndla_msservices_get_site_id($fields['site']);

  $return = array();
  $data = array();

  $query = db_select('ndla_annotate_server', 'nas')
    ->fields('nas', array('page_data'))
    ->condition('page', $page, '=')
    ->condition('site_id', $site_id, '=')
    ->condition('uid', $fields['uid'], '=');

  $result = $query->execute();

  while ($data = $result->fetchAssoc()) {
    $return[] = json_decode($data['page_data']);
  }

  $data = array();
  $data['page'] = array(
    'ndla_annotation' => $return,
  );

  return $data;
}

/**
 * Implementation of hook_msservices_set_NAMESPACE()
 */
function ndla_annotate_server_msservices_set_ndla_annotation($fields) {
  $page = $fields['language'] . '/' . $fields['page'];

  // Delete existing annotations for the page
  db_delete('ndla_annotate_server')
    ->condition('site_id', $fields['site_id'])
    ->condition('uid', $fields['uid'])
    ->condition('page', $page)
    ->execute();

  $annotations = json_decode($fields['data']);
  foreach($annotations as $annotation) {
    $id = db_insert('ndla_annotate_server')
      ->fields(array(
        'site_id' => $fields['site_id'],
        'uid' => $fields['uid'],
        'page' => $page,
        'url' => '',
        'page_data' => json_encode($annotation),
      ))
      ->execute();
  }

  // Invalidate all cached versions
  $cid = "annotate_widget:" . $fields['uid'] . ":";
  cache_clear_all($cid, 'cache', TRUE);
}

/**
 * Implementation of hook_msservices_delete_NAMESPACE()
 */
function ndla_annotate_server_msservices_delete_ndla_annotation($data, $site_id) {
  $page = $fields['language'] . '/' . $fields['page'];

  // Delete existing annotations for the page
  db_delete('ndla_annotate_server')
    ->condition('site_id', $fields['site_id'])
    ->condition('uid', $fields['uid'])
    ->condition('page', $page)
    ->execute();

  // Invalidate all cached versions
  $cid = "annotate_widget:" . $fields['uid'] . ":";
  cache_clear_all($cid, 'cache', TRUE);
}

function ndla_annotate_server_get_annotations() {
  global $user;
  $result = db_query(
    "SELECT * FROM {ndla_annotate_server} nas
    INNER JOIN {ndla_msservices_sites} nms ON nas.site_id = nms.site_id
    WHERE uid = :uid", array(':uid' => $user->uid)
  );
  $rows = array();
  foreach ($result as $thread) {
    $annotations = json_decode($thread->page_data);
    if(!is_array($annotations)) {
      preg_match('/^([^\/]*)\/node/', $thread->page, $matches);
      $lang = $matches[1];
      $rows[] = array(
        'url' => $thread->url . '/' . $thread->page,
        'title' => $annotations->title,
        'text' => $annotations->text,
        'id' => $thread->id,
        'language' => $lang,
      );
    } else {
      /**
       * Deprecated annotations
       */
      foreach($annotations as $annotation) {
        $rows[] = array(
          'url' => $thread->url . '/' . $thread->page,
          'title' => $annotation->title,
          'text' => $annotation->text,
          'id' => $thread->id,
        );
      }
    }
  }
  return $rows;
}