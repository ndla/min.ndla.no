<h1><?php echo drupal_get_title();?></h1>
<div id='ndla_contents'>
  <p><?php echo $text;?></p>
  <form method='post' id='form' onsubmit='$(".deletables").removeAttr("disabled");'>
    <input type='hidden' name='my_xss' id='xss' value=''>
    <input type='hidden' name='action' value='delete_widget'>

<?php
  echo '    <input type="checkbox" onclick="if (this.checked) $(\'.deletables\').attr(\'checked\', \'checked\').attr(\'disabled\', \'disabled\'); else $(\'.deletables\').removeAttr(\'disabled\').removeAttr(\'checked\');" value="" id="delete_all" name="delete_all"> <label for="delete_all">' . t('Delete all data including your user account') . '</label>' . "\n";

  foreach ($deletables as $deletable) {
    echo '    <div><input type="checkbox" data-url="' . $deletable->url . '" class="deletables" value="' . $deletable->name . '" id="' . $deletable->name . '" name="deletable[]"> <label for="' . $deletable->name . '">' . t($deletable->title) . '</label></div>' . "\n";
  }

?>
    <input type='hidden' id='roam_auth_url' name='rau'>
    <p><button type='button' onclick="deleteMe();"><?php echo t("Delete me");?></button></p>
  </form>
</div>

<script type="text/javascript">
  function deleteMe() {
    a = confirm(Drupal.t('Are you sure? The data will be lost, no undo posibilities.'));
    if (!a) return false;
    var xss = Math.random();
    $('#xss').val(xss)

    var host_name = document.location.hostname;
    var parts = host_name.split('.');
    var main_host_name = parts[parts.length - 2] + '.' + parts[parts.length - 1];

    document.cookie = 'my_xss=' + xss + ';path=/;domain=' + main_host_name;

    $('#roam_auth_url').val(getRoamAuthUrl());
    $('#form').submit();
  }
</script>
