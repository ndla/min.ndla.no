<h1><?php echo drupal_get_title();?></h1>
<p><?php print t('Add or remove login providers from your account.');?>
<p>
<?php
  $rows = array();

  foreach ($additional_providers as $provider) {
    $rows[] = array(
      'data' => array(
        ($provider['logo'] ?
          theme_image(array(
            'path' => $provider['logo'],
            'alt' => t('Login provider for @provider', array('@provider' => $provider['name'])),
            'title' => t('Login provider for @provider', array('@provider' => $provider['name'])),
            'attributes' => '',
          ))
          :
          $provider['name']
        ),
        theme_button(array('element' => array('#value' => t('Add'), '#button_type' => 'button', '#attributes' => array(
          'onclick' => 'document.location.href="' . $provider['add_url'] . '"',
        )))),
      ),
    );
  }

  foreach ($user_providers as $provider) {
    $rows[] = array(
      'data' => array(
        ($provider['logo'] ?
          theme_image(array(
            'path' => $provider['logo'],
            'alt' => t('Login provider for @provider', array('@provider' => $provider['name'])),
            'title' => t('Login provider for @provider', array('@provider' => $provider['name'])),
            'attributes' => '',
          ))
          :
          $provider['name']
        ),
        theme_button(array('element' => array('#value' => t('Delete'), '#button_type' => 'button'))),
      ),
    );
  }


  print theme_table(array(
    'header' => array(
    ),
    'rows' => $rows,
    'attributes' => array('id' => 'login_providers'),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => '',
    'empty' => '',
  ));
?>
</p>
