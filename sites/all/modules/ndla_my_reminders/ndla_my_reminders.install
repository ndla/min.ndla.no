<?php

/* Implementation of hook_schema() */
function ndla_my_reminders_schema() {
  $schema['ndla_my_reminders'] = array(
    'description' => 'Reminder dates for ndla_my_reminders',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for the reminder',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'site_id' => array(
        'type' => 'int',
        'description' => 'Key identifying the site the data belongs to (optional)',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'description' => 'Key identifying the user the reminder belongs to',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'page' => array(
        'description' => 'The path of the page this data belongs to (optional).',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'url' => array(
        'description' => 'The url to the reminder (optional)',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'page_data' => array(
        'description' => 'Reminder page data.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'note' => array(
        'description' => 'Reminder note text.',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'email_notify' => array(
        'type' => 'int',
        'description' => 'If reminder should be emailed',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'email_sent' => array(
        'type' => 'int',
        'description' => 'If reminder has been emailed',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'notification_read' => array(
        'type' => 'int',
        'description' => 'If the notification is read',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description'  => 'Timestamp for when reminder should fire',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'all_read' => array('email_notify', 'email_sent', 'notification_read'),
      'notify' => array('page', 'site_id', 'uid'),
      'site_id' => array('site_id'),
      'timestamp' => array('timestamp'),
      'user' => array('uid'),
      'notification_read' => array('notification_read'),
    ),
  );
  return $schema;
}
