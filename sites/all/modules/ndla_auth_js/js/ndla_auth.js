function ndla_auth_object(protocol, host, callback) {
  var caller = this;
  caller.user_info = false;

  var authcom = getExternalAuthServer(protocol, host);

  this.login_url = function(return_url) {
    if (return_url == undefined) return_url = document.URL;
    return (Drupal.settings.ndla_auth.enabled ? '/seriaauth/authenticate?continue=' + escape(return_url) : authcom.loginUrl(return_url));
  };

  this.logout_url = function(return_url) {
    if (return_url == undefined) return_url = window.location.protocol + '//' + window.location.hostname;
    return (Drupal.settings.ndla_auth.enabled ? '/user/logout' : authcom.logoutUrl(return_url));
  };

  this.user_settings_url = function() {
    return Drupal.settings.ndla_auth.settings_url;
  };

  this.display_name = function() {
    if (!this.authenticated()) return false;
    return caller.user_info.displayName;
  };

  this.is_teacher = function() {
    if (!this.authenticated()) return false;
    for (i in caller.user_info.extensionValues.ndlaRoles) {
      if (caller.user_info.extensionValues.ndlaRoles[i] == 'teacher') return true;
    }
    return false;
  };

  this.is_student = function() {
    if (!this.authenticated()) return false;
    for (i in caller.user_info.extensionValues.ndlaRoles) {
      if (caller.user_info.extensionValues.ndlaRoles[i] == 'student') return true;
    }
    if (caller.user_info.extensionValues.ndlaRoles.length == 0) return true; // If the user has no roles, we assume that the user is a student
    return false;
  };

  this.first_name = function() {
    if (!this.authenticated()) return false;
    return caller.user_info.firstName;
  };

  this.last_name = function() {
    if (!this.authenticated()) return false;
    return caller.user_info.lastName;
  };

  this.surname = this.last_name;

  this.authenticated = function() {
    return (Drupal.settings.ndla_auth.enabled ? (Drupal.settings.logged_in == true && caller.user_info.loggedIn != undefined && caller.user_info.loggedIn == true) : (caller.user_info.loggedIn != undefined && caller.user_info.loggedIn == true));
  };

  this.is_authenticated = this.authenticated;

  this.profile_picture = function(width, height, method) {
    if (!this.authenticated()) return false;
    if (!caller.user_info.extensionValues.hasAvatarPicture) return false;
    var params = (width != undefined && width ? 'width=' + width + '&' : '') + (height != undefined && height ? 'height=' + height + '&' : '') + (method != undefined ? 'method=' + method : '');
    return protocol + '://' + host + '/?route=seria%2FndlaAvatar%2FavatarImage&' + params;
  };

  authcom.getUserInfo(function(data) {
    caller.user_info = data;
    ndla_auth_is_initializing = false;
    return callback(caller);
  });

}

var ndla_auth_cache;
var ndla_auth_is_initializing = false;

function ndla_auth(callback) {
  if (jQuery.isReady && !ndla_auth_is_initializing) {
    if (ndla_auth_cache != undefined) {
      callback(ndla_auth_cache);
    } else {
      ndla_auth_is_initializing = true;
      ndla_auth_cache = new ndla_auth_object(Drupal.settings.ndla_auth.scheme, Drupal.settings.ndla_auth.host, callback);
    }
  } else {
    setTimeout(function() {
      ndla_auth(callback);
    }, 100);
  }
}
