<?php
function ndla_msservices_config() {
  $form = array('#tree' => TRUE);
  $sites = db_query("SELECT * FROM {ndla_msservices_sites}");
  foreach ($sites as $site) {
    $empty = FALSE;
    $form['excisting_sites']['site' . $site->site_id] = array(
      '#title' => $site->title,
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    ndla_msservices_add_site_fields($form['excisting_sites']['site' . $site->site_id], $site);
  }
  $form['new_site'] = array(
    '#title' => t('New site'),
    '#type' => 'fieldset',
  );
  ndla_msservices_add_site_fields($form['new_site']);
  $form['save'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );
  return $form;
}

function ndla_msservices_config_validate($form, &$form_state) {
  $urls = array();
  if (isset($form_state['values']['excisting_sites'])) {
    foreach ($form_state['values']['excisting_sites'] as $site) {
      if (!$site['delete']) {
        if (!$site['title']) {
          form_set_error('excisting_sites][site' . $site['site_id'] . '][title', t('A site must have a title'));
        }
        if (!$site['url'] || !valid_url($site['url'])) {
          form_set_error('excisting_sites][site' . $site['site_id'] . '][url', t('A site must have a valid url'));
        }
        else {
          if (in_array($site['url'], $urls)) {
            form_set_error('excisting_sites', t('The urls must be unique'));
          }
          $urls[] = $site['url'];
        }
      }
    }
  }
  if ($form_state['values']['new_site']['title'] xor $form_state['values']['new_site']['url']) {
    form_set_error('new_site', t('A site must have both a title and a url'));
  }
  elseif ($form_state['values']['new_site']['url'] && !valid_url($form_state['values']['new_site']['url'])) {
    form_set_error('new_site][url', t('You must use a valid url.'));
  }
  elseif (in_array($form_state['values']['new_site']['url'], $urls)) {
    form_set_error('new_site][url', t('The urls must be unique'));
  }
}

function ndla_msservices_config_submit($form, &$form_state) {
  if ($form_state['values']['new_site']['title'] && $form_state['values']['new_site']['url']) {
    drupal_write_record('ndla_msservices_sites', $form_state['values']['new_site']);
  }
  if (isset($form_state['values']['excisting_sites'])) {
    foreach ($form_state['values']['excisting_sites'] as $site) {
      if ($site['delete']) {
        db_delete('ndla_msservices_sites')
          ->condition('site_id', $site['site_id'])
          ->execute();
      }
      else {
        drupal_write_record('ndla_msservices_sites', $site, 'site_id');
      }
    }
  }
}

function ndla_msservices_add_site_fields(&$form, $defaults = NULL) {
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#description' => t('The title of this site as it will appear in the Min Side GUI'),
    '#default_value' => isset($defaults->title) ? $defaults->title : '',
  );
  $form['url'] = array(
    '#title' => t('Url'),
    '#type' => 'textfield',
    '#description' => t('The url of this site will be used in links and also to identify webservice requests'),
    '#default_value' => isset($defaults->url) ? $defaults->url : '',
  );
  if (isset($defaults)) {
    $form['delete'] = array(
      '#title' => t('Delete'),
      '#type' => 'checkbox',
      '#description' => t('Mark this checkbox in order to delete the site and all user data associated with the site.')
    );
    $form['site_id'] = array(
      '#type' => 'value',
      '#value' => $defaults->site_id,
    );
  }
}