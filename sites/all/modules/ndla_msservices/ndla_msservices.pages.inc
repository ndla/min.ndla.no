<?php
/* 
 * @file
 *  Handles page requests from other ndla sites
 *
 *  TODO: SECURITY
 *  TODO: PERFORMANCE
 */

function ndla_msservices_check_in($params) {
  global $user;

  if (isset($params['site'], $params['page'], $params['title'])) {
    ndla_msservices_log_visit($params);

    $site_id = ndla_msservices_get_site_id($params['site']);

    $params['site_id'] = $site_id;
    $params['uid'] = $user->uid;

    $data = module_invoke_all('msservices_get', $params);
    ndla_msservices_cors_output($data);
  }
  else {
    ndla_msservices_cors_output(array('success' => 0));
  }
}

function ndla_msservices_set($params) {
  global $user;
  $site_id = ndla_msservices_get_site_id($params['site']);

  if ($site_id && isset($params['title'], $params['namespace'], $params['scope'], $params['data'])) {
    $params['site_id'] = $site_id;
    $params['uid'] = $user->uid;

    if (!in_array($params['scope'], array('global', 'site', 'page'))) {
      ndla_msservices_cors_output(array('success' => 0));
    }

    module_invoke_all('msservices_set_' . check_plain($params['namespace']), $params);

    ndla_msservices_cors_output(array('success' => 1));
  }
  else {
    ndla_msservices_cors_output(array('success' => 0));
  }
}


function ndla_msservices_get($params) {
  global $user;
  $site_id = ndla_msservices_get_site_id($params['site']);

  if (isset($params['namespace'], $params['site'], $params['scope'])) {
    $params['site_id'] = $site_id;
    $params['uid'] = $user->uid;

    $data = module_invoke_all('msservices_conditional_get_' . check_plain($params['namespace']), $params);
    $data['success'] = 1;
  }
  else {
    $data = array('success' => 0);
  }

  ndla_msservices_cors_output($data);
}


function ndla_msservices_delete($params) {
  global $user;
  $site_id = ndla_msservices_get_site_id($params['site']);

  $params['site_id'] = $site_id;
  $params['uid'] = $user->uid;

  module_invoke_all('msservices_delete_' . check_plain($params['namespace']), $params);
  ndla_msservices_cors_output(array('success' => 1));
}

function ndla_msservices_get_site_id($site_url) {
  return db_query('SELECT site_id FROM {ndla_msservices_sites} WHERE url = :url', array(':url' => $site_url))->fetchField();
}

function ndla_msservices_log_visit($params) {
  global $user;
  db_insert('ndla_msservices_visits')
    ->fields(array(
      'uid' => $user->uid,
      'url' => $params['site'] . '/' . $params['language'] . '/' . $params['page'],
      'title' => $params['title'],
      'timestamp' => REQUEST_TIME,
    ))
    ->execute();
}

function ndla_msservices_cors_output($data) {
  // Make sure only domains added in the module's admin pages are allowed to
  // get CORS content
  $allowed_domain = ndla_msservices_get_site_id($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : "";

  $json = str_replace('\\/', '/', json_encode($data));
  drupal_add_http_header("Access-Control-Allow-Origin", $allowed_domain);
  drupal_add_http_header("Access-Control-Allow-Credentials", "true");
  echo $json;
}
