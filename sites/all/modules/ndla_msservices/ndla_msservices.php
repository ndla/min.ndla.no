<?php
/**
 * @file
 *  Communicates with other ndla sites.
 *
 * This file does drupal bootstrap manually, because we need to be able to change logged in user before
 * the request is handled to the internal functions of the module.
 */

/*
 * NOTE: To use this file you need to have your module in sites/all/modules/. You can't put it in a subfolder.
 * AND:  You have to define $base_url in settings.php to use this module. This in order to get the same
 * session name when you go through this file and index.php...
 */

include_once 'ndla_msservices.pages.inc';
chdir('../../../../');
define('DRUPAL_ROOT', getcwd());
include_once "includes/bootstrap.inc";
include_once "includes/common.inc";


// Because of the broken CORS implementation in IE 8/9, we must use HTTP POST request 
// with Content-Type: text/plain. Because of this the $_POST variable is not populated
// automatically, so we do it manually here.
if (isset($HTTP_RAW_POST_DATA)) {
  parse_str($HTTP_RAW_POST_DATA, $_POST);
}

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (($GLOBALS['user']->uid == 0) && !empty($_POST['msservices_op']) && !empty($_POST['msservices_roamauth'])) {
  // Note: IE 8/9 will always trigger this, because the CORS implementation in those
  // browsers does not allow sending cookies, so the user variable will never be set.
  seriaauth_roamauth($_POST['msservices_roamauth']);
}

if (!isset($GLOBALS['user'], $GLOBALS['user']->uid)) {
  ndla_msservices_cors_output(array('success' => 0));
  exit;
}

if (!user_access('access ndla_msservices')) {
  ndla_msservices_cors_output(array('success' => 0));
  exit();
}

// Remove auth parameters from $_POST before sending the data
// to the internal methods
$op = $_POST['msservices_op'];
unset($_POST['msservices_op']);
unset($_POST['msservices_roamauth']);

switch ($op) {
  case 'check_in':
    ndla_msservices_check_in($_POST);
    break;
  case 'set':
    ndla_msservices_set($_POST);
    break;
  case 'get':
    ndla_msservices_get($_POST);
    break;
  case 'delete':
    ndla_msservices_delete($_POST);
    break;
}
