<?php

function ndla_notify_config() {
  $form = array();

  $form['frame'] = array(
    '#type' => 'fieldset',
    '#title' => 'Default welcome message',
    '#description' => 'This information is sent as a notification to all users when they sign up to ndla.no',
  );

  $form['frame']['ndla_notify_welcome_title'] = array(
    '#title' => 'Title',
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_notify_welcome_title', ''),
  );

  $form['frame']['ndla_notify_welcome_url'] = array(
    '#title' => 'URL',
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_notify_welcome_url', ''),
  );

  $form['frame']['ndla_notify_welcome_message'] = array(
    '#title' => 'Message',
    '#type' => 'textarea',
    '#default_value' => variable_get('ndla_notify_welcome_message', ''),
  );

  return system_settings_form($form);
}
