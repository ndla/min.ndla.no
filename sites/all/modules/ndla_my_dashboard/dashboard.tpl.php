<div id='widget-list'>
<?php foreach($widgets as $widget): ?>
  <section class='widget' data-url='<?php print $widget->url; ?>' data-id='<?php print $widget->id; ?>'>
	  <header><a href='javascript:void(0);' class='fa fa-search' title='<?php print t('Search'); ?>'></a><i class='fa fa-ellipsis-v'></i><?php print t($widget->title); ?></header>
    <div class='search'><input type='text' placeholder='<?php print t('Search here!'); ?>'></div>
		<div class='content'>
      <div class='status throbber'></div>
    <div>
	</section>
<?php endforeach; ?>
</div>