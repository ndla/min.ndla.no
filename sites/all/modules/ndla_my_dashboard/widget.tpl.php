<div id='widget_page'>  
  <section class='widget' data-url='<?php print $url; ?>' data-id='<?php print $widget_id; ?>'>
    <header><?php print t($title); ?></header>
    <div class='search'><label><?php print t('Search') . ':'?></label> <input type='text'></div>
  	<div class='content'>
      <div class='status throbber'></div>
    <div>
  </section>
</div>