jQuery(document).ready(function($) {
  
  /* Extend contains to be case insensitive */
  $.extend($.expr[':'], {
    'containsi': function(elem, i, match, array)
    {
      return (elem.textContent || elem.innerText || '').toLowerCase()
      .indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });
  
  /* Set up sortable widgets */
  $('#widget-list').sortable({
    placeholder: "widget drop" ,
    handle: '.fa-ellipsis-v',
    update: function( event, ui ) {
      var order = [];
      $('#widget-list section.widget').each(function(index,element) {
        order.push($(element).data('id'));
      });
      $.ajax({
        type: 'POST',
        url: Drupal.settings.basePath + 'dashboard/settings/order',
        data: {'order' : order}
      });
    }
  });
  
  /* Setup search button */
  $('#widget-list header .fa-search').click(function() {
    $element = $(this).parent().siblings('.search');
    $panel = $(this).parent().siblings('.content').find('.scroll');
    if($element.is(':visible')) {
      $element.animate(
        {height: "-=27px"}, 500, function() {
          $(this).hide();
        }
      );
      $panel.animate(
        {height: "+=28px"}, 500
      );
    } else {
      $element.show().animate(
        {height: "+=27px"}, 500
      );
      $panel.animate(
        {height: "-=28px"}, 500
      );
    }
    $element.find('input').focus();
  });

  /* Setup table filter */
  $('.widget div.search input').keyup(function () {
      var value = this.value.split(" ");
      var rows = $(this).parents('section.widget').find('tbody tr');
      if (this.value == '') {
          rows.show().removeClass('odd').removeClass('even');
          $(this).parents('section.widget').find('tbody tr:visible').each(function(key, element) {
            if(key%2) {
              $(element).addClass('even');
            } else {
              $(element).addClass('odd');
            }
          });
          return;
      }
      rows.hide();
      rows.filter(function (i, v) {
        var $t = $(this);
        if ($t.is(":containsi('" + value + "')")) {
          return true;
        }
        return false;
      }).show();
      rows.removeClass('odd').removeClass('even');
      $(this).parents('section.widget').find('tbody tr:visible').each(function(key, element) {
        if(key%2) {
          $(element).addClass('even');
        } else {
          $(element).addClass('odd');
        }
      });
  });

  loadWidget = function($widget, data) {
    if (typeof data == 'undefined') {
      data = Object();
    }
    data['rau'] = getRoamAuthUrl();
    data['view'] = (jQuery('#widget_page').length == 1) ? 'full' : 'normal';
    data['widget_id'] = $widget.data('id');
    data['lang'] = Drupal.settings.ndla_my_dashboard.language;

    var url = $widget.data('url');
    var $content = $widget.find('.content');
    $.ajax({
      url: url,
      dataType: 'jsonp',
      data: data,
      crossDomain: true,
      success: function(json) {
        if(typeof json.type != undefined) {
          switch(json.type) {
            case 'table':
              buildTable($content, json.data);
              $widget.find('.search input').trigger('keyup');
              break;
          }
        }
        if($content.find('table').length == 0) {
          $content.find('.status').removeClass('throbber').addClass('error');
        }
      },
      error: function() {
        $content.find('.status').removeClass('throbber').addClass('error');
      }
    });
  }
  
  /* Get widget contents */
  $('.widget').each(function() {
    loadWidget($(this));
  });
  
  buildTable = function($contents, data) {
    var chop = Drupal.settings.ndla_my_dashboard.chop;
    var widget_id = $contents.parents('section.widget').data('id');
    data.widget_id = widget_id;
    var template = "\
<div class='scroll'>\
  <table>\
    <thead>\
      <tr>\
        {{headers}}\
          <th {{if sortable}}class='sortable'{{/if}}>\
            <span>{{if text|notempty}}{{text}}{{/if}}</span>\
          </th>\
        {{/headers}}\
      </tr>\
    </thead>\
    <tbody>\
      {{rows}}\
      <tr>\
        {{.}}\
        <td {{if lang|notempty}}lang='{{lang}}' xml:lang='{{lang}}'{{/if}}>\
          {{if link}}\
            <a href='{{link}}' target='_new'>{{text|chop>" + chop + "}}</a>\
          {{else}}\
            {{if action}}\
              <button data-action='{{action}}' data-params='{{params}}'>{{text|chop>" + chop + "}}</button>\
            {{else}}\
              {{if sort}}<span class='hidden-sort'>{{sort}}</span>{{/if}}{{if full}}{{text}}{{else}}{{text|chop>" + chop + "}}{{/if}}\
            {{/if}}\
          {{/if}}\
        </td>\
        {{/.}}\
      </tr>\
      {{/rows}}\
    </tbody>\
  </table>\
</div>\
{{if more}}<footer><a href='/dashboard/detail/{{widget_id}}'>{{more}}</a></footer>{{/if}}";

    var result = Mark.up(template, data);
    $contents.html(result);
    
    var sortable = {};
    $(data.headers).each(function(key, header){
      if(typeof header.sortable == 'undefined' || header.sortable == false) {
        sortable[key] = {sorter: false};
      }
    });    
    $contents.find('table').tablesorter({
      headers : sortable,
      widgets: ['zebra']
    });
    if(jQuery('#widget_page').length == 0) {
      $contents.find('.scroll').jScrollPane({ verticalGutter: 0 });
    }
    $contents.find('button').click(function() {
      $widget = $(this).parents('section.widget');
      
      var params = $(this).data('params');
      var data = Object();
      data['action'] = $(this).data('action');
      if (params) {
        var tmp = Array();
        data['params'] = {};
        parse_str(params, tmp);
          for (key in tmp) {
            data['params'][key] = tmp[key];
          }
      }
      loadWidget($widget, data);
    });
  }

  function parse_str (str, array) {
    // http://kevin.vanzonneveld.net
    // +   original by: Cagri Ekin
    // +   improved by: Michael White (http://getsprink.com)
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // +   reimplemented by: stag019
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: stag019
    // +   input by: Dreamer
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
    // +   input by: Zaide (http://zaidesthings.com/)
    // +   input by: David Pesta (http://davidpesta.com/)
    // +   input by: jeicquest
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // %        note 1: When no argument is specified, will put variables in global scope.
    // %        note 1: When a particular argument has been passed, and the returned value is different parse_str of PHP. For example, a=b=c&d====c
    // *     example 1: var arr = {};
    // *     example 1: parse_str('first=foo&second=bar', arr);
    // *     results 1: arr == { first: 'foo', second: 'bar' }
    // *     example 2: var arr = {};
    // *     example 2: parse_str('str_a=Jack+and+Jill+didn%27t+see+the+well.', arr);
    // *     results 2: arr == { str_a: "Jack and Jill didn't see the well." }
    // *     example 3: var abc = {3:'a'};
    // *     example 3: parse_str('abc[a][b]["c"]=def&abc[q]=t+5');
    // *     results 3: JSON.stringify(abc) === '{"3":"a","a":{"b":{"c":"def"}},"q":"t 5"}';


    var strArr = String(str).replace(/^&/, '').replace(/&$/, '').split('&'),
      sal = strArr.length,
      i, j, ct, p, lastObj, obj, lastIter, undef, chr, tmp, key, value,
      postLeftBracketPos, keys, keysLen,
      fixStr = function (str) {
        return decodeURIComponent(str.replace(/\+/g, '%20'));
      };

    if (!array) {
      array = this.window;
    }

    for (i = 0; i < sal; i++) {
      tmp = strArr[i].split('=');
      key = fixStr(tmp[0]);
      value = (tmp.length < 2) ? '' : fixStr(tmp[1]);

      while (key.charAt(0) === ' ') {
        key = key.slice(1);
      }
      if (key.indexOf('\x00') > -1) {
        key = key.slice(0, key.indexOf('\x00'));
      }
      if (key && key.charAt(0) !== '[') {
        keys = [];
        postLeftBracketPos = 0;
        for (j = 0; j < key.length; j++) {
          if (key.charAt(j) === '[' && !postLeftBracketPos) {
            postLeftBracketPos = j + 1;
          }
          else if (key.charAt(j) === ']') {
            if (postLeftBracketPos) {
              if (!keys.length) {
                keys.push(key.slice(0, postLeftBracketPos - 1));
              }
              keys.push(key.substr(postLeftBracketPos, j - postLeftBracketPos));
              postLeftBracketPos = 0;
              if (key.charAt(j + 1) !== '[') {
                break;
              }
            }
          }
        }
        if (!keys.length) {
          keys = [key];
        }
        for (j = 0; j < keys[0].length; j++) {
          chr = keys[0].charAt(j);
          if (chr === ' ' || chr === '.' || chr === '[') {
            keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
          }
          if (chr === '[') {
            break;
          }
        }

        obj = array;
        for (j = 0, keysLen = keys.length; j < keysLen; j++) {
          key = keys[j].replace(/^['"]/, '').replace(/['"]$/, '');
          lastIter = j !== keys.length - 1;
          lastObj = obj;
          if ((key !== '' && key !== ' ') || j === 0) {
            if (obj[key] === undef) {
              obj[key] = {};
            }
            obj = obj[key];
          }
          else { // To insert new dimension
            ct = -1;
            for (p in obj) {
              if (obj.hasOwnProperty(p)) {
                if (+p > ct && p.match(/^\d+$/g)) {
                  ct = +p;
                }
              }
            }
            key = ct + 1;
          }
        }
        lastObj[key] = value;
      }
    }
  }

});
