<?php

function ndla_my_dashboard_admin_settings_form_submit($form, &$form_state) {
  $value = $form_state['input'];
  // Existing plugins
  if(isset($value['plugins'])) {
    foreach($value['plugins'] as $id => $manifest_url) {
      if($manifest_url) {
        if($manifest = ndla_my_dashboard_get_manifest($manifest_url)) {
          if($manifest['name'] == $id) {
            ndla_my_dashboard_add_plugin($manifest, $manifest_url);
          } else {
            drupal_set_message(t("The name of the plugin has changed. This is not supported. Old name was '@old_name' and the new name is '@new_name'", array('@old_name' => $manifest['name'], '@new_name' => $id)), 'error');
            return FALSE;
          }
        }
      } else {
        ndla_my_dashboard_remove_plugin($id);
      }
    }
  }
  // New plugins
  if(isset($value['new_plugins'])) {
    foreach($value['new_plugins'] as $manifest_url) {
      if($manifest = ndla_my_dashboard_get_manifest($manifest_url)) {
        ndla_my_dashboard_add_plugin($manifest, $manifest_url);
      }
    }
  }
  if($value['default_dashboard']) {
    variable_set('ndla_my_dashboard_default_dashboard', $value['default_dashboard']);
  }
  if($value['chop_length']) {
    variable_set('ndla_my_dashboard_chop_length', $value['chop_length']);
  }
}

function ndla_my_dashboard_admin_settings_form($form, &$form_state) {
  $options = array();
  $form['chop_length'] = array(
    '#title' => t('Column chop length'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_my_dashboard_chop_length', 20),
  );
  $dashboards = db_query('SELECT id, title FROM {ndla_my_dashboard_dashboard} WHERE shared=1')->fetchAll();
  foreach($dashboards as $dashboard) {
    $options[$dashboard->id] = $dashboard->title;
  }
  $form['default_dashboard'] = array(
    '#title' => t('Default dashboard'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('ndla_my_dashboard_default_dashboard', 1),
  );
  $form['plugin_urls'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugins'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );
  // Get plugins from db
  $plugins = db_query('SELECT id, manifest_url FROM {ndla_my_dashboard_plugin}');
  foreach($plugins as $plugin) {
    $form['plugin_urls']['plugins'][$plugin->id] = array(
      '#name' => 'plugins[' . $plugin->id . ']',
      '#title' => t('Plugin url'),
      '#type' => 'textfield',
      '#default_value' => $plugin->manifest_url,
    );
  }
  $form['plugin_urls']['new_plugins'] = array(
    '#markup' => '<div id="ndla-my-dashboard-new-plugins"></div>',
  );
  $form['plugin_urls']['add_plugin'] = array(
    '#value' => t('Add new plugin'),
    '#type' => 'button',
    '#ajax' => array(
      'callback' => 'ndla_my_dashboard_add_plugin_field',
      'wrapper' => 'ndla-my-dashboard-new-plugins',
      'method' => 'append',
      'effect' => 'fade',
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function ndla_my_dashboard_add_plugin_field($form, &$form_state) {
  return array(
    '#name' => 'new_plugins[]',
    '#title' => t('Plugin url'),
    '#type' => 'textfield',
    '#default_value' => 'http://',
  );
}