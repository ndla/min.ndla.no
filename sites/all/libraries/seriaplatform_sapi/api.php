<?php

function platformsapilibs__http_post($url, $data)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data, '', '&'));
	$res = curl_exec($curl);
	if(curl_errno($curl)){
		seriaplatform_sapi_error('Curl error: ' . curl_error($curl));
		return null;
	}
	curl_close($curl);
	return $res;
}

function platformsapilibs__http_get($url)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($curl);
	if(curl_errno($curl)){
		seriaplatform_sapi_error('Curl error: ' . curl_error($curl));
		return null;
	}
	curl_close($curl);
	return $res;
}

function platformsapilibs__unmagic_quotes($params)
{
	$process = array(&$params);
	while (list($key, $val) = each($process)) {
		foreach ($val as $k => $v) {
			unset($process[$key][$k]);
			if (is_array($v)) {
				$process[$key][stripslashes($k)] = $v;
				$process[] = &$process[$key][stripslashes($k)];
			} else {
				$process[$key][stripslashes($k)] = stripslashes($v);
			}
		}
	}
	unset($process);
	return $params;
}

function seriaplatform_sapi_secure($data, $secret)
{
	$hash = 'sha256'; /* http://php.net/manual/en/function.hash-algos.php
	                   * http://en.wikipedia.org/wiki/Cryptographic_hash_function
	                   * http://www.php.net/manual/en/function.hash-hmac.php
	                   * http://en.wikipedia.org/wiki/HMAC
	                   * 
	                   * Md5 and sha1 are probably acceptable because hmac is not
	                   * particularly affected by collisions in the underlying hash
	                   * function. (Sha2 hash-functions (sha224, sha256, sha384, sha512) are
	                   * not (yet known to be) vulnerable)
	                   */
	$data['apiTime'] = time();
	$data['apiSalt'] = substr(md5(mt_rand()), 0, 6);
	$data['apiHash'] = $hash;
	$message = http_build_query($data, '', '&');
	$data['apiAuth'] = hash_hmac($hash, $message, $secret);
	return $data;
}

/**
 * 
 * Post to the new Seria Platform API.
 * @param string $url The url to post to.
 * @param string $apiPath The API path to post to.
 * @param array $data The data to post.
 * @param string $secret The secret app-key (shared secret).
 * @return array The response, or null on failure.
 */
function seriaplatform_sapi_post($url, $apiPath, $data, $secret=null)
{
	if ($secret === '') {
		seriaplatform_sapi_error('Tried Seria Platform SAPI with blank secret!');
		return null;
	}
	/*
	 * Split the $url into:
	 *  $baseurl - Url without params and fragment.
	 *  $additionalParams - Url-parameters.
	 */
	$urlp = parse_url($url);
	$baseurl = $urlp['scheme'].'://';
	if (isset($urlp['user']) && $urlp['user']) {
		$baseurl .= $urlp['user'];
		if (isset($urlp['pass']) && $urlp['pass'])
			$baseurl .= ':'.$urlp['pass'];
		$baseurl .= '@';
	}
	$baseurl .= $urlp['host'];
	if (isset($urlp['port']) && $urlp['port'])
		$baseurl .= ':'.$urlp['port'];
	$baseurl .= $urlp['path'];
	if (isset($urlp['query'])) {
		$additionalParams = array();
		parse_str($urlp['query'], $additionalParams);
		if (get_magic_quotes_gpc())
			$additionalParams = platformsapilibs__unmagic_quotes($additionalParams);
	} else
		$additionalParams = array();
	$additionalParams['apiPath'] = $apiPath;

	/*
	 * If the post will be authenticated by hmac.
	 */
	if ($secret !== null) {
		/* Url authentication */
		$additionalParams = seriaplatform_sapi_secure($additionalParams, $secret);

		/*
		 * Secure the post parameters:
		 */
		$data = seriaplatform_sapi_secure($data, $secret);
	}

	/*
	 * Construct the final url with all parameters:
	 */
	$query = http_build_query($additionalParams, '', '&');
	$url = $baseurl.'?'.$query;

	$data = platformsapilibs__http_post($url, $data);
	$data = json_decode($data, true);
	if (is_string($data))
		return $data;
	if (isset($data['error']) && $data['error']) {
		seriaplatform_sapi_error($data['error']);
		return null;
	}
	return $data;
}

/**
 *
 * Query the new Seria Platform API.
 * @param string $url The url to fetch.
 * @param string $apiPath The API path to query.
 * @param array $data Additional url-parameters
 * @param string $secret The secret app-key (shared secret)
 * @return array The response, or null on failure.
 */
function seriaplatform_sapi_get($url, $apiPath, $data=array(), $secret=null)
{
	if ($secret === '') {
		seriaplatform_sapi_error('Tried Seria Platform SAPI with blank secret!');
		return null;
	}
	/*
	 * Split the $url into:
	 *  $baseurl - Url without params and fragment.
	 *  $additionalParams - Url-parameters.
	 */
	$urlp = parse_url($url);
	$baseurl = $urlp['scheme'].'://';
	if (isset($urlp['user']) && $urlp['user']) {
		$baseurl .= $urlp['user'];
		if (isset($urlp['pass']) && $urlp['pass'])
			$baseurl .= ':'.$urlp['pass'];
		$baseurl .= '@';
	}
	$baseurl .= $urlp['host'];
	if (isset($urlp['port']) && $urlp['port'])
		$baseurl .= ':'.$urlp['port'];
	$baseurl .= $urlp['path'];
	if (isset($urlp['query'])) {
		$additionalParams = array();
		parse_str($urlp['query'], $additionalParams);
		if (get_magic_quotes_gpc())
			$additionalParams = platformsapilibs__unmagic_quotes($additionalParams);
	} else
		$additionalParams = array();
	$additionalParams['apiPath'] = $apiPath;

	/*
	 * Append the data parameters.
	 */
	foreach ($data as $name => $value) {
		$additionalParams[$name] = $value;
	}
	$data = $additionalParams;

	/*
	 * If the query will be authenticated:
	 */
	if ($secret !== null)
		$data = seriaplatform_sapi_secure($data, $secret);

	/*
	 * Construct the final url with all parameters:
	 */
	$query = http_build_query($data, '', '&');
	$url = $baseurl.'?'.$query;

	$data = platformsapilibs__http_get($url);
	$data = json_decode($data, true);
	if (is_string($data))
		return $data;
	if (isset($data['error']) && $data['error']) {
		seriaplatform_sapi_error($data['error']);
		return null;
	}
	return $data;
}

/**
 *
 * Get or set the error string. Use this to retrieve the error string after a failed API-call.
 * The error will be cleared automatically after reading.
 *
 * @param string $set Optional new error string.
 */
function seriaplatform_sapi_error($set=null)
{
	static $error = null;

	$value = $error;
	$error = $set;
	return $value;
}
